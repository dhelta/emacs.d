;; UTF-8 as default encoding
(set-language-environment "UTF-8")
;;show line numbers
(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))
;; Mac version : use Meta for M
(setq mac-command-modifier 'meta mac-option-modifier 'none default-input-method "MacOSX")
;;no tool bar, no scroll bar
(if (featurep 'tool-bar)
  (tool-bar-mode -1))
(if (featurep 'scroll-bar)
    (scroll-bar-mode -1))
;;windmove
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))
;;disable Org Mode keybindings for Shift+arrow keys
(setq org-replace-disputed-keys t)
;; no bell sound
(setq visible-bell 1)  

(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
  ;; and `package-pinned-packages`. Most users will not need or want to do this.
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  )
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (dracula)))
 '(custom-safe-themes
   (quote
    ("55c2069e99ea18e4751bd5331b245a2752a808e91e09ccec16eb25dadbe06354" "5f1bd7f67dc1598977e69c6a0aed3c926f49581fdf395a6246f9bc1df86cb030" default)))
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
    (auto-package-update ess xml-format json-reformat all-the-icons neotree dashboard evil dracula-theme))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; dashboard
(require 'dashboard)
(dashboard-setup-startup-hook)

;; dashboard : set the title
(setq dashboard-banner-logo-title "Time to break out that emacs and edit that files")
;; dashboard :set the banner
(setq dashboard-startup-banner 'official)

;;neotree
(add-to-list 'load-path "/some/path/neotree")
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)

;; Enable elpy
(elpy-enable)
;;ipython as interpreter
(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt")
;;org-agenda-files
(setq org-agenda-files (list "~/Documents/todo.org"))




